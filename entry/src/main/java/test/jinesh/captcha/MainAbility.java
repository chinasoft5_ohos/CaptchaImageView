package test.jinesh.captcha;

import test.jinesh.captcha.slice.MainAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * MainAbility
 * @since 2021-07-02
 */
public class MainAbility extends Ability {
    /**
     * onStart
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
