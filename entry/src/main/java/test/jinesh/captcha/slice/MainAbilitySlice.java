package test.jinesh.captcha.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.bundle.AbilityInfo;

import com.muddzdev.styleabletoast.styleabletoast.StyleableToast;

import test.jinesh.captcha.ResourceTable;
import test.jinesh.captchaimageviewlib.CaptchaImageView;

/**
 * 入口
 *
 * @since 2021-07-02
 */
public class MainAbilitySlice extends AbilitySlice {
    Image refreshButton;
    TextField captchaInput;
    Text submitButton;
    CaptchaImageView captchaImageView;

    /**
     * onStart
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getAbility().getWindow().setStatusBarColor(Color.getIntColor("#303F9F"));
        refreshButton = (Image) findComponentById(ResourceTable.Id_regen);
        captchaInput = (TextField) findComponentById(ResourceTable.Id_captchaInput);
        submitButton = (Text) findComponentById(ResourceTable.Id_submitButton);
        captchaImageView = (CaptchaImageView) findComponentById(ResourceTable.Id_image);
        captchaImageView.setCaptchaType(CaptchaImageView.CaptchaGenerator.BOTH);
//        captchaImageView.setIsDotNeeded(true);
        captchaInput.requestFocus();
        captchaInput.setTouchFocusable(true);
        refreshButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                captchaImageView.regenerate();
            }
        });
        submitButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (captchaInput.getText().toString().equals(captchaImageView.getCaptchaCode())) {
                    new StyleableToast.Builder(getContext())
                        .text("Matched")
                        .textColor(new Color(Color.getIntColor("#333333")))
                        .backgroundColor(new Color(Color.getIntColor("#F0F0F0")))
                        .show();
                } else {
                    new StyleableToast.Builder(getContext())
                        .text("Not Matching")
                        .textColor(new Color(Color.getIntColor("#333333")))
                        .backgroundColor(new Color(Color.getIntColor("#F0F0F0")))
                        .show();
                }
            }
        });

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.LINE); // 设置背景类型
        RgbColor rgbColor = RgbColor.fromArgbInt(Color.getIntColor("#FF4081"));
        shapeElement.setRgbColors(new RgbColor[]{rgbColor, rgbColor}); // 设置颜色值 起始与结束时的颜色
        shapeElement.setStroke(AttrHelper.vp2px(30, this), rgbColor); // 设置光标高度
        captchaInput.setCursorElement(shapeElement); // 设置光标背景色
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
        if (captchaInput != null) {
            captchaInput.requestFocus();
            captchaInput.setTouchFocusable(true);
        }
    }

    /**
     * onOrientationChanged
     * @param displayOrientation displayOrientation
     */
    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        captchaImageView.isRedraw(false);
        captchaInput.clearFocus();
        captchaInput.requestFocus();
    }

    /**
     * onForeground
     * @param intent onForeground
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
