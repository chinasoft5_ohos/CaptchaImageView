# CaptchaImageView

#### 项目介绍
- 项目名称：CaptchaImageView
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义ImageView以生成验证码图像
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.0


#### 效果演示
![Output sample](https://gitee.com/chinasoft5_ohos/CaptchaImageView/raw/master/screens/captcha_screen.gif) ![Output sample](https://gitee.com/chinasoft5_ohos/CaptchaImageView/raw/master/screens/captcha_screen_2.gif)

#### 安装教程
```
// 添加maven仓库 
repositories { 
    maven { 
        url 'https://s01.oss.sonatype.org/content/repositories/releases/' 
    }
}

// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:CaptchaImageView:1.0.0'   
}
```

#### 使用说明
```xml
            <test.jinesh.captchaimageviewlib.CaptchaImageView
                ohos:id="$+id:image"
                ohos:height="50vp"
                ohos:width="match_parent"/>
```
对CaptchaImageView调用regenerate（）方法以重新生成captcha

```java
 captchaImageView= (CaptchaImageView) findComponentById(ResourceTable.Id_image);
refreshButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                captchaImageView.regenerate();
            }
        });
```

调用CaptchaImageView上的getCaptchaCode（）方法来读取最后生成的captcha代码。

```java
 captchaImageView.getCaptchaCode()

```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.0.0

#### 版权和许可信息
[Apache License, Version 2.0](LICENSE.md)

