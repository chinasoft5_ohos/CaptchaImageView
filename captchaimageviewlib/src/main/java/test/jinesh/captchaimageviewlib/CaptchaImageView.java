package test.jinesh.captchaimageviewlib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Texture;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Position;
import ohos.media.image.common.Size;

import java.security.SecureRandom;

/**
 * 验证码图片
 *
 * @since 2021-06-25
 */
public class CaptchaImageView extends Image implements Component.EstimateSizeListener {
    /**
     * 斜体
     */
    public static final int TYPE_ITALIC = 100;
    /**
     * 键入纯文本
     */
    public static final int TYPE_PLAIN_TEXT = 101;
    private CaptchaGenerator.Captcha generatedCaptcha;
    private int captchaLength = 6;
    private int captchaType = CaptchaGenerator.NUMBERS;
    private int mWidth;
    private int mHeight;
    private boolean isDot;
    private boolean isRedraw;
    private boolean isItalic = true;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    /**
     * 创建实例
     *
     * @param context context
     */
    public CaptchaImageView(Context context) {
        super(context);
    }

    /**
     * 创建实例
     *
     * @param context context
     * @param attrSet attrSet
     */
    public CaptchaImageView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * 创建实例
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public CaptchaImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
        setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                if (!isRedraw) {
                    reDraw();
                    isRedraw = true;
                }
            }
        });
    }

    /**
     * onEstimateSize
     *
     * @param widthMeasureSpec widthMeasureSpec
     * @param heightMeasureSpec heightMeasureSpec
     * @return onEstimateSize
     */
    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 300;
        int desiredHeight = 50;
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        int width;
        int height;
        if (widthMode == EstimateSpec.PRECISE) {
            width = widthSize;
        } else if (widthMode == EstimateSpec.NOT_EXCEED) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }
        if (heightMode == EstimateSpec.PRECISE) {
            height = heightSize;
        } else if (heightMode == EstimateSpec.NOT_EXCEED) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }
        this.mWidth = width;
        this.mHeight = height;
        setEstimatedSize(
            Component.EstimateSpec.getChildSizeWithMode(width, width, widthMode),
            Component.EstimateSpec.getChildSizeWithMode(height, height, heightMode));
        return true;
    }

    private void draw(int width, int height) {
        generatedCaptcha = CaptchaGenerator.regenerate(width, height, captchaLength, captchaType, isDot, isItalic);
    }

    /**
     * isRedraw
     *
     * @param isRedraw isRedraw
     */
    public void isRedraw(boolean isRedraw) {
        this.isRedraw = isRedraw;
    }

    /**
     * Returns the generated captcha bitmap image
     *
     * @return Generated Bitmap
     */
    public PixelMap getCaptchaBitmap() {
        return generatedCaptcha.getBitmap();
    }

    /**
     * Returns the generated captcha code
     *
     * @return captcha string
     */
    public String getCaptchaCode() {
        return generatedCaptcha.getCaptchaCode();
    }


    /**
     * Regenerates the captcha again to result a new bitmap and captcha code
     */
    public void regenerate() {
        reDraw();
    }

    /**
     * Sets the type of captcha need to generate.Default value is CaptchaGenerator.NUMBER
     *
     * @param type Type of the captcha
     * CaptchaGenerator.NUMBER - Generates a captcha with only numbers
     * CaptchaGenerator.ALPHABETS - Generates a captcha with only numbers
     * CaptchaGenerator.BOTH - Generates a captcha with both numbers and letter
     */
    public void setCaptchaType(int type) {
        captchaType = type;
    }

    /**
     * Sets the style of captcha need to generate.Default value is TYPE_ITALIC
     *
     * @param style Style of the captcha
     * CaptchaImageView.TYPE_ITALIC - Generates a captcha with italic style
     * CaptchaImageView.TYPE_PLAIN_TEXT - Generates a captcha with plain text style
     */
    public void setTextStyle(int style) {
        switch (style) {
            case TYPE_ITALIC:
                isItalic = true;
                break;
            case TYPE_PLAIN_TEXT:
                isItalic = false;
                break;
            default:
                isItalic = true;
                break;
        }
    }

    /**
     * Sets the desired length of captcha need to generate
     *
     * @param length length of captcha
     */
    public void setCaptchaLength(int length) {
        captchaLength = length;
    }

    /**
     * Method to set Background dots
     *
     * @param isNeeded pass true if dots needed false otherwise
     */
    public void setIsDotNeeded(boolean isNeeded) {
        isDot = isNeeded;
    }

    /**
     * Redraws the captcha
     */
    private void reDraw() {
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                if (mWidth != 0 && mHeight != 0) {
                    draw(mWidth, mHeight);
                    setPixelMap(generatedCaptcha.getBitmap());
                }
            }
        });
    }

    /**
     * 验证码生成器
     *
     * @since 2021-07-02
     */
    public static class CaptchaGenerator {
        /**
         * 字母表
         */
        public static final int ALPHABETS = 1;
        /**
         * 数字
         */
        public static final int NUMBERS = 2;
        /**
         * 数字和字母表都包含
         */
        public static final int BOTH = 3;

        private static Captcha regenerate(int width, int height, int length,
                                          int type, boolean isDot, boolean isItalic) {
            Paint border = new Paint();
            border.setStyle(Paint.Style.STROKE_STYLE);
            border.setColor(new Color(Color.getIntColor("#CCCCCC")));
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
            if (isDot) {
                paint.setFont(Font.DEFAULT_BOLD);
            } else {
                paint.setFont(Font.MONOSPACE);
            }
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            options.editable = true;
            PixelMap bitMap = PixelMap.create(options);
            Texture texture = new Texture(bitMap);
            Canvas canvas = new Canvas(texture);
            canvas.drawColor(Color.getIntColor("#F7F7FF"), BlendMode.SRC_OVER);
            int textX = generateRandomInt(width - ((width / 5) * 4), width / 2);
            int textY = generateRandomInt(height - ((height / 3)), height - (height / 4));
            if (isDot) {
                canvas.drawLine(textX, textY - generateRandomInt(7, 10), textX + (length * 33),
                    textY - generateRandomInt(5, 10), paint);
                canvas.drawLine(textX, textY - generateRandomInt(7, 10), textX + (length * 33),
                    textY - generateRandomInt(5, 10), paint);
            } else {
                canvas.drawLine(textX, textY - generateRandomInt(7, 10), textX + (length * 23),
                    textY - generateRandomInt(5, 10), paint);
                canvas.drawLine(textX, textY - generateRandomInt(7, 10), textX + (length * 23),
                    textY - generateRandomInt(5, 10), paint);
            }
            canvas.drawRect(0, 0, width - 1, height - 1, border);
            String generatedText = drawRandomText(canvas, paint, textX, textY, length, type, isDot, isItalic);
            if (isDot) {
                makeDots(bitMap, width, height, textX, textY);
            }
            return new Captcha(generatedText, bitMap);
        }

        private static void makeDots(PixelMap bitMap, int width, int height, int textX, int textY) {
            int white = -526337;
            int black = -16777216;
            int grey = -3355444;
            SecureRandom random = new SecureRandom();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int pixel = bitMap.readPixel(new Position(x, y));
                    if (pixel == white) {
                        pixel = (random.nextBoolean()) ? black : white;
                    }
                    bitMap.writePixel(new Position(x, y), pixel);
                }
            }
        }

        private static String drawRandomText(Canvas canvas, Paint paint, int textX, int textY,
                                             int length, int type, boolean isDot, boolean isItalic) {
            StringBuilder generatedCaptcha = new StringBuilder();
            int[] scewRange = {-1, 1};
            int[] textSizeRange = {40, 42, 44, 45};
            SecureRandom random = new SecureRandom();
            paint.horizontalTilt(isItalic ? scewRange[random.nextInt(scewRange.length)] : 0);
            for (int index = 0; index < length; index++) {
                String temp = generateRandomText(type);
                generatedCaptcha.append(temp);
                paint.setTextSize(textSizeRange[random.nextInt(textSizeRange.length)]);
                if (isDot) {
                    canvas.drawText(paint, temp, textX + (index * 25), textY);
                } else {
                    canvas.drawText(paint, temp, textX + (index * 20), textY);
                }
            }
            return generatedCaptcha.toString();
        }

        private static String generateRandomText(int type) {
            String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] alphabets = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            SecureRandom random = new SecureRandom();
            SecureRandom mixedRandom = new SecureRandom();
            String temp;
            if (type == ALPHABETS) {
                temp = alphabets[random.nextInt(alphabets.length)];
            } else if (type == NUMBERS) {
                temp = numbers[random.nextInt(numbers.length)];
            } else {
                temp = mixedRandom.nextBoolean() ? alphabets[random.nextInt(alphabets.length)]
                    : numbers[random.nextInt(numbers.length)];
            }
            return temp;
        }

        private static int generateRandomInt(int min, int max) {
            SecureRandom rand = new SecureRandom();
            return rand.nextInt((max - min) + 1) + min;
        }

        /**
         * 验证码
         *
         * @since 2021-07-02
         */
        private static class Captcha {
            private String captchaCode;
            private PixelMap bitmap;

            /**
             * Captcha
             * @param captchaCode captchaCode
             * @param bitmap bitmap
             */
            Captcha(String captchaCode, PixelMap bitmap) {
                this.captchaCode = captchaCode;
                this.bitmap = bitmap;
            }

            /**
             * getCaptchaCode
             * @return String
             */
            String getCaptchaCode() {
                return captchaCode;
            }

            /**
             * setCaptchaCode
             * @param captchaCode captchaCode
             */
            public void setCaptchaCode(String captchaCode) {
                this.captchaCode = captchaCode;
            }

            /**
             * getBitmap
             * @return PixelMap
             */
            PixelMap getBitmap() {
                return bitmap;
            }

            /**
             * setBitmap
             * @param bitmap bitmap
             */
            public void setBitmap(PixelMap bitmap) {
                this.bitmap = bitmap;
            }
        }
    }
}
